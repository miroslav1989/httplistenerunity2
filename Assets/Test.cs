﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

    [SerializeField] UnityHttpListener UnityHttpListener;

    // Use this for initialization
    void Start () {
        UnityHttpListener.RequestReceived += e => Debug.Log($"Log from test {e}");
    }
}